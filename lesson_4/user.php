<?php

class User {

	public $email;
	public $password;
	CONST MINCHARS = 8;

	public function login(){
		return 'Loggin in ...';
	}

	public function logout(){
		return 'Loggin out ...';
	}

	public function setPassword($string){
		if(strlen($string) < self::MINCHARS) {
			try {
			    throw new Exception('Password should be ' . self::MINCHARS . ' characters long');
			} catch(Exception $e) {
			    echo $e->getMessage();
			}
		} else {
			$this->password = hash('sha256', $string);
		}

	}

}

?>